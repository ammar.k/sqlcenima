
/*Des données pour tester*/

INSERT INTO GENRE(libelle) VALUES('Action'); /*1*/
INSERT INTO GENRE(libelle) VALUES('Aventure'); /*2*/
INSERT INTO GENRE(libelle) VALUES('Comédie'); /*3*/
INSERT INTO GENRE(libelle) VALUES('Drame'); /*4*/
INSERT INTO GENRE(libelle) VALUES('Fantastique'); /*5*/
INSERT INTO GENRE(libelle) VALUES('Horreur'); /*6*/
INSERT INTO GENRE(libelle) VALUES('Policier'); /*7*/
INSERT INTO GENRE(libelle) VALUES('Science-fiction'); /*8*/
INSERT INTO GENRE(libelle) VALUES('Thriller'); /*9*/

INSERT INTO ROLE(role) VALUES('Batman'); /*1*/
INSERT INTO ROLE(role) VALUES('Superman'); /*2*/
INSERT INTO ROLE(role) VALUES('Spiderman'); /*3*/
INSERT INTO ROLE(role) VALUES('Ironman'); /*4*/
INSERT INTO ROLE(role) VALUES('Hulk'); /*5*/
INSERT INTO ROLE(role) VALUES('Thor'); /*6*/
INSERT INTO ROLE(role) VALUES('Captain America'); /*7*/
INSERT INTO ROLE(role) VALUES('Black Widow'); /*8*/
INSERT INTO ROLE(role) VALUES('William Wallace'); /*9*/
INSERT INTO ROLE(role) VALUES('Gandalf'); /*10*/
INSERT INTO ROLE(role) VALUES('Maximus'); /*11*/
INSERT INTO ROLE(role) VALUES('Luke Skywalker'); /*12*/
INSERT INTO ROLE(role) VALUES('jack Dawson'); /*13*/
INSERT INTO ROLE(role) VALUES('lois lane'); /*14*/
INSERT INTO ROLE(role) VALUES('Martha Kent'); /*15*/
INSERT INTO ROLE(role) VALUES('Princess Isabelle'); /*16*/
INSERT INTO ROLE(role) VALUES('Lucilla'); /*17*/
INSERT INTO ROLE(role) VALUES('Frodo'); /*18*/
INSERT INTO ROLE(role) VALUES('Rose'); /*19*/
INSERT INTO ROLE(role) VALUES('Princess Leia'); /*20*/
INSERT INTO ROLE(role) VALUES('Han Solo'); /*21*/
INSERT INTO ROLE(role) VALUES('Joker'); /*22*/
INSERT INTO ROLE(role) VALUES('Murray'); /*23*/
INSERT INTO ROLE(role) VALUES('Sophie Dumond'); /*24*/
INSERT INTO ROLE(role) VALUES('Roi Edward'); /*25*/



/*acteur/actrice*/

INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Affleck','Ben','1972-08-15','M'); --1 id_personne
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Cavill','Henry','1983-05-05','M'); --2
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Garfield','Andrew','1983-08-20','M'); --3
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Downey Jr.','Robert','1965-04-04','M'); --4
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Ruffalo','Mark','1967-11-22','M'); --5
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Hemsworth','Chris','1983-08-11','M'); --6
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Evans','Chris','1981-06-13','M'); --7
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Johansson','Scarlett','1984-11-22','F'); --8
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Gibson','Mel','1956-01-03','M');  --9
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('McKellen','Ian','1939-05-25','M'); --10
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('DiCaprio','Leonardo','1974-11-11','M'); --21
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Crowe','Russell','1964-04-07','M'); --22
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Hamill','Marc','1942-07-13','M'); --23
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Adams','Amy','1974-08-20','F'); --24
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Lane','Diane','1965-01-22','F'); --25
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Marceau','Sophie','1966-11-17','F'); --26
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Nielsen','Connie','1965-07-03','F'); --27
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Wood','Elijah','1981-01-28','M'); --28
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Winslet','Kate','1975-10-05','F'); --29
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Fisher','Carrie','1956-10-21','F'); --30
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Ford','Harrison','1942-07-13','M'); --31
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Phoenix','Joaquin','1974-10-28','M'); --33
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('De Niro','Robert','1943-08-17','M'); --34
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Beetz','Zazie','1991-05-25','F'); --35
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('McGoohan','Patrick','1928-03-19','M'); --36



/*réalisateur*/

INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Snyder','Zack','1966-03-01','M'); --11 id_personne
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Raimi','Sam','1959-10-23','M'); --12
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Scott','Ridley','1937-11-30','M'); --13
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Nolan','Christopher','1970-07-30','M'); --14
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Jackson','Peter','1961-10-31','M'); --15
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Cameron','James','1954-08-16','M'); --16
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Spielberg','Steven','1946-12-18','M'); --17
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Tarantino','Quentin','1963-03-27','M'); --18
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Lucas','George','1944-05-14','M'); --19
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Columbus','Chris','1958-09-10','M'); --20
INSERT INTO `PERSONNE`(`nom`, `prenom`, `dateNaissance`, `sex`) VALUES ('Philips','todd','1970-12-20','M'); --32 id_personne


/*acteurs*/
INSERT INTO `ACTEUR`(`id_personne`) VALUES (1); --1 id_acteur
INSERT INTO `ACTEUR`(`id_personne`) VALUES (2); --2
INSERT INTO `ACTEUR`(`id_personne`) VALUES (3); --3
INSERT INTO `ACTEUR`(`id_personne`) VALUES (4); --4
INSERT INTO `ACTEUR`(`id_personne`) VALUES (5); --5
INSERT INTO `ACTEUR`(`id_personne`) VALUES (6); --6
INSERT INTO `ACTEUR`(`id_personne`) VALUES (7); --7
INSERT INTO `ACTEUR`(`id_personne`) VALUES (8); --8
INSERT INTO `ACTEUR`(`id_personne`) VALUES (9); --9
INSERT INTO `ACTEUR`(`id_personne`) VALUES (10); --10
INSERT INTO `ACTEUR`(`id_personne`) VALUES (21); --11
INSERT INTO `ACTEUR`(`id_personne`) VALUES (22); --12
INSERT INTO `ACTEUR`(`id_personne`) VALUES (23); --13
INSERT INTO `ACTEUR`(`id_personne`) VALUES (24); --14
INSERT INTO `ACTEUR`(`id_personne`) VALUES (25); --15
INSERT INTO `ACTEUR`(`id_personne`) VALUES (26); --16
INSERT INTO `ACTEUR`(`id_personne`) VALUES (27); --17
INSERT INTO `ACTEUR`(`id_personne`) VALUES (28); --18
INSERT INTO `ACTEUR`(`id_personne`) VALUES (29); --19
INSERT INTO `ACTEUR`(`id_personne`) VALUES (30); --20
INSERT INTO `ACTEUR`(`id_personne`) VALUES (31); --21
INSERT INTO `ACTEUR`(`id_personne`) VALUES (33); --22
INSERT INTO `ACTEUR`(`id_personne`) VALUES (34); --23
INSERT INTO `ACTEUR`(`id_personne`) VALUES (35); --24
INSERT INTO `ACTEUR`(`id_personne`) VALUES (36); --25



/*réalisateurs*/

INSERT INTO `REALISATEUR`(`id_personne`) VALUES (11); --1 id_realisateur
INSERT INTO `REALISATEUR`(`id_personne`) VALUES (12); --2
INSERT INTO `REALISATEUR`(`id_personne`) VALUES (13); --3
INSERT INTO `REALISATEUR`(`id_personne`) VALUES (14); --4
INSERT INTO `REALISATEUR`(`id_personne`) VALUES (15); --5
INSERT INTO `REALISATEUR`(`id_personne`) VALUES (16); --6
INSERT INTO `REALISATEUR`(`id_personne`) VALUES (17); --7
INSERT INTO `REALISATEUR`(`id_personne`) VALUES (18); --8
INSERT INTO `REALISATEUR`(`id_personne`) VALUES (19); --9
INSERT INTO `REALISATEUR`(`id_personne`) VALUES (20); --10
INSERT INTO `REALISATEUR`(`id_personne`) VALUES (9); --11
INSERT INTO `REALISATEUR`(`id_personne`) VALUES (32); --12

/*film*/
INSERT INTO `FILM`(`titre`, `annee_sortie`, `duree`, `redume`, `note`, `affiche_film`, `id_realisateur`) 
VALUES ('Batman v Superman : L’Aube de la Justice','2016','151','Le face à face tant attendu 
entre Superman et Batman, deux des plus grands super-héros de tous les temps.','3','batmanvssuperman.jpg','1'); --1 id_film
INSERT INTO `FILM`(`titre`, `annee_sortie`, `duree`, `redume`, `note`, `affiche_film`, `id_realisateur`)
VALUES ('Brave Heart','1995','177','Évocation de la vie tumultueuse de William Wallace, héros et symbole de lindépendance écossaise, 
qui à la fin du XIIIe siècle affronta les troupes du roi dAngleterre Edward I qui venaient denvahir son pays.','5','braveheart.jpg','9'); --2

INSERT INTO `FILM`(`titre`, `annee_sortie`, `duree`, `redume`, `note`, `affiche_film`, `id_realisateur`)
VALUES ('Gladiator','2000','155','Le général romain Maximus est le plus fidèle soutien de lempereur Marc Aurèle',5,'gladiator.jpg','3'); --3

INSERT INTO `FILM`(`titre`, `annee_sortie`, `duree`, `redume`, `note`, `affiche_film`, `id_realisateur`)
VALUES ('Le Seigneur des anneaux : La Communauté de lanneau','2001','178','Le jeune et timide Hobbit, Frodon Sacquet',5,'seigneurdesanneaux.jpg','5'); --4

INSERT INTO `FILM`(`titre`, `annee_sortie`, `duree`, `redume`, `note`, `affiche_film`, `id_realisateur`)
VALUES ('Titanic','1997','194',' Southampton, 10 avril 1912. Le paquebot le plus grand et le plus moderne du monde, réputé pour son insubmersibilité',5,'titanic.jpg','6'); --5

INSERT INTO `FILM`(`titre`, `annee_sortie`, `duree`, `redume`, `note`, `affiche_film`, `id_realisateur`)
VALUES ('Star Wars, épisode IV : Un nouvel espoir','1977','121','Il y a bien longtemps, dans une galaxie très lointaine',5,'starwars.jpg','9'); --6

-- joker
INSERT INTO `FILM`(`titre`, `annee_sortie`, `duree`, `redume`, `note`, `affiche_film`, `id_realisateur`)
VALUES ('Joker','2019','122','Le film, qui relate une histoire originale inédite sur grand écran, se focalise sur la figure emblématique de l’ennemi juré de Batman.',5,'joker.jpg','12'); --7


/*acter*/
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (1,1,1);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (1,2,2);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (2,9,9);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (3,12,11);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (4,10,10);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (5,11,13);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (6,13,12);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (1,14,14);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (1,15,15);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (2,16,16);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (3,17,17);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (4,18,18);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (5,19,19);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (6,20,20);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (6,21,21);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (7,22,22);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (7,23,23);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (7,24,24);
INSERT INTO `acter`(`id_film`, `id_acteur`, `id_role`) VALUES (7,25,25);



/*appartenir*/

INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (1,1);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (1,8);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (1,9);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (2,4);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (2,9);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (3,4);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (3,9);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (4,2);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (4,5);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (4,8);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (5,2);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (5,4);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (6,8);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (6,9);
INSERT INTO `appartenir`(`id_film`, `id_genre`) VALUES (7,4);

