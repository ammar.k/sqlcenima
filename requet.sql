/* les requetes */

/* -- 1 -- Informations d’un film (id_film) : titre, année, durée (au format HH:MM) et réalisateur.*/

SELECT titre, annee_sortie, SEC_TO_TIME(duree*60) AS duree, PERSONNE.nom, PERSONNE.prenom 
FROM FILM 
INNER JOIN REALISATEUR ON FILM.id_realisateur = REALISATEUR.id_realisateur 
INNER JOIN PERSONNE ON PERSONNE.id_personne = REALISATEUR.id_personne 
WHERE id_film = 1;

/* -- 2 -- Liste des films dont la durée excède 2h15 classés par durée (du + long au + court)*/

SELECT titre, SEC_TO_TIME(duree*60) AS duree
FROM FILM
WHERE duree > 135
ORDER BY duree DESC;

/* -- 3 -- Liste des films d’un réalisateur (en précisant l’année de sortie)*/

SELECT PERSONNE.nom, PERSONNE.prenom, titre, annee_sortie 
FROM FILM 
INNER JOIN REALISATEUR ON FILM.id_realisateur=REALISATEUR.id_realisateur 
INNER JOIN PERSONNE ON PERSONNE.id_personne=REALISATEUR.id_personne 
WHERE REALISATEUR.id_realisateur = 9;

/* -- 4 -- Nombre de films par genre (classés dans l’ordre décroissant)*/

SELECT libelle, COUNT(id_film) AS number_film
FROM GENRE
INNER JOIN appartenir ON GENRE.id_genre=appartenir.id_genre
GROUP BY appartenir.id_genre
ORDER BY number_film DESC;

/* -- 5 -- Nombre de films par réalisateur (classés dans l’ordre décroissant)*/

SELECT PERSONNE.nom, PERSONNE.prenom, COUNT(id_film) AS number_film
FROM REALISATEUR
INNER JOIN PERSONNE ON REALISATEUR.id_personne=PERSONNE.id_personne
INNER JOIN FILM ON REALISATEUR.id_realisateur=FILM.id_realisateur
GROUP BY REALISATEUR.id_realisateur
ORDER BY number_film DESC;

/* -- 6 -- Casting d’un film en particulier (id_film) : nom, prénom des acteurs + sexe*/

SELECT FILM.titre, PERSONNE.nom,PERSONNE.prenom,PERSONNE.sex 
FROM acter 
INNER JOIN FILM on acter.id_film=FILM.id_film 
INNER JOIN ACTEUR ON ACTEUR.id_acteur=acter.id_acteur 
INNER JOIN PERSONNE ON PERSONNE.id_personne=ACTEUR.id_personne;
WHERE FILM.id_film = 1;

/* -- 7 -- Films tournés par un acteur en particulier (id_acteur) avec leur rôle et l’année de
sortie (du film le plus récent au plus ancien)*/

SELECT FILM.titre, ROLE.role, FILM.annee_sortie
FROM acter
INNER JOIN FILM ON acter.id_film=FILM.id_film
INNER JOIN ACTEUR ON ACTEUR.id_acteur=acter.id_acteur
INNER JOIN ROLE ON ROLE.id_role=acter.id_role
WHERE ACTEUR.id_acteur = 1


/* -- 8 -- Liste des personnes qui sont à la fois acteurs et réalisateurs */

SELECT PERSONNE.nom, PERSONNE.prenom
FROM PERSONNE
INNER JOIN ACTEUR ON PERSONNE.id_personne=ACTEUR.id_personne
INNER JOIN REALISATEUR ON PERSONNE.id_personne=REALISATEUR.id_personne;


/* -- 9 -- Liste des films qui ont moins de 5 ans (classés du plus récent au plus ancien) */

SELECT titre, annee_sortie
FROM FILM
WHERE annee_sortie > YEAR(NOW()) - 5
ORDER BY annee_sortie DESC;

/* -- 10 -- Nombre d’hommes et de femmes parmi les acteurs */

SELECT sex, COUNT(sex) as total 
FROM PERSONNE 
INNER JOIN ACTEUR ON ACTEUR.id_personne=PERSONNE.id_personne 
GROUP BY sex;


/* -- 11 -- Liste des acteurs ayant plus de 50 ans (âge révolu et non révolu) */

SELECT nom, prenom, YEAR(NOW()) - YEAR(dateNaissance) AS age
FROM PERSONNE
INNER JOIN ACTEUR ON PERSONNE.id_personne=ACTEUR.id_personne
WHERE YEAR(NOW()) - YEAR(dateNaissance) > 50;


/* -- 12 -- Acteurs ayant joué dans 3 films ou plus */

SELECT nom, prenom, COUNT(acter.id_film) AS number_film
FROM PERSONNE
INNER JOIN ACTEUR ON PERSONNE.id_personne=ACTEUR.id_personne
INNER JOIN acter ON ACTEUR.id_acteur=acter.id_acteur
GROUP BY ACTEUR.id_acteur
HAVING number_film >= 3;